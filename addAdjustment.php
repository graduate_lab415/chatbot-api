<?php
/**
 * 002 [POST] /Chatbot/addAdjustment.php
 * 當使用者選擇其他順位的問題時，紀錄選項，以供系統後續優化。
 * 
 * q: 使用者輸入的問句
 * qa_id: 使用者認為較貼切的問句，若都不符合則傳送 -1
 * category_id: 使用者選擇的類別編號
 */
header('Content-Type: application/json; charset=utf-8');
$q = $_POST['q'];
$qa_id = $_POST['qa_id'];
$category_id = $_POST['category_id'];
$output = exec('/home/yr/PycharmProjects/nlp/venv/bin/python3 /home/yr/PycharmProjects/nlp/add_adjustment.py ' . $q . ' ' . $qa_id . ' ' . $category_id .' 2>/home/yr/PycharmProjects/nlp/output/log.txt', $output2);
// print_r(error_get_last());
// var_dump($output2);
print($output);