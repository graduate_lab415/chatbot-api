<?php
/**
 * 001 [POST] /Chatbot/getAnswer.php
 * 以使用者輸入的語句查詢，回傳資料集中與輸入最相似的 3 筆問答組合。
 * 
 * q: 使用者輸入的問句
 * category_id: 使用者選擇的類別編號
 */
header('Content-Type: application/json; charset=utf-8');
$q = $_POST['q'];
$category_id = $_POST['category_id'];
$output = exec('/home/yr/PycharmProjects/nlp/venv/bin/python3 /home/yr/PycharmProjects/nlp/main.py ' . $q . ' ' . $category_id . ' 2>/home/yr/PycharmProjects/nlp/output/log.txt', $output2);
// print_r(error_get_last());
// print($output2);
print($output);